#Airpods Bluetooth low volume 
sudo nano /lib/systemd/system/bluetooth.service
Change this line:
ExecStart=/usr/lib/bluetooth/bluetoothd

To this:
ExecStart=/usr/lib/bluetooth/bluetoothd --plugin=a2dp

Restart the daemon and the Bluetooth service:
sudo systemctl daemon-reload
sudo systemctl restart bluetooth
https://www.jeroenmoonen.nl/blog/algemeen/apple-airpods-has-a-low-volume-on-ubuntu-18-04/
