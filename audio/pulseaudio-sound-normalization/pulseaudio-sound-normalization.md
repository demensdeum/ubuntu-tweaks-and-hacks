# Pulseaudio Sound Normalization
You can enable upfront sound normalization for Pulseaudio by this instruction:
https://gist.github.com/lightrush/4fc5b36e01db8fae534b0ea6c16e347f  
https://askubuntu.com/questions/95716/automatically-adjust-the-volume-based-on-content

Quote:  
Install LADSPA plugins containing the compressor and limiter we'll use:  
sudo apt install swh-plugins
Install PulseAudio Preferences paprefs:  
sudo apt install paprefs

Open PulseAudio Preferences:  
paprefs

Go to the Simultaneous Output tab.  
Check Add virtual output ... all sound cards and Close.

Copy default.pa from this gist to ~/.config/pulse/.

Restart PulseAudio:  
pulseaudio -k

You can check how if it works by this video:    
https://www.youtube.com/watch?v=n56WQ_640-4