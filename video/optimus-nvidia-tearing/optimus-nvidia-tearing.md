# Nvidia Optimus Screen Tearing
To fix nvidia's screen tearing in Ubuntu you can use fix from here:  
https://askubuntu.com/questions/1079135/screen-tearing-on-ubuntu-18-04/1110975#1110975

Quote:  
For optimus nvidia you need to use prime sync. In a terminal create this file.  
sudo nano /etc/modprobe.d/zz-nvidia-modeset.conf

Insert this  
options nvidia_drm modeset=1

Then run this command  
sudo update-initramfs -u  

Reboot 